# The documentation for the contents of this file can be found at:
# https://docs.gitlab.com/ce/ci/yaml/README.html

# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/rust/tags/
# The recipe for this docker image can be found at:
# https://github.com/rust-lang/docker-rust/blob/21171fdd92e29acb045a41cd58b0d30d66aeaa7f/1.54.0/buster/Dockerfile
image: "rust:1.54.0"

variables:
  CARGO_HOME: $CI_PROJECT_DIR/.cargo
  # Build without debug information enabled to decrease compilation time
  # and binary sizes in CI. This option is assumed to only have marginal
  # effects on the generated code, likely only in terms of section
  # arrangement. See
  # https://doc.rust-lang.org/cargo/reference/environment-variables.html
  # https://doc.rust-lang.org/rustc/codegen-options/index.html#debuginfo
  RUSTFLAGS: '-C debuginfo=0'

.crates-io-cache: &crates-io-cache
  key: crates-io-cache
  paths:
    # See https://doc.rust-lang.org/cargo/guide/cargo-home.html#caching-the-cargo-home-in-ci
    - $CARGO_HOME/bin/
    - $CARGO_HOME/git/db/
    - $CARGO_HOME/registry/index/
    - $CARGO_HOME/registry/cache/
  policy: pull-push

.target-debug-native-cache: &target-debug-native-cache
  key: target-debug-native-cache-$CI_JOB_IMAGE
  paths:
    - target/debug/
    - Cargo.lock
  policy: pull-push

.target-debug-wasm-cache: &target-debug-wasm-cache
  key: target-debug-wasm-cache-$CI_JOB_IMAGE
  paths:
    - wasm-bindgen-test-runner/bin/
    - wasm-bindgen-test-runner/version
    - wasm-bindgen-test-runner/wasm-bindgen.tar.gz
    - target/debug/
    - target/wasm32-unknown-unknown/debug/
    - Cargo.lock
  policy: pull-push

.target-release-native-cache: &target-release-native-cache
  key: target-release-native-cache-$CI_JOB_IMAGE
  paths:
    - target/release/
    - Cargo.lock
  policy: pull-push

.target-release-wasm-cache: &target-release-wasm-cache
  key: target-release-wasm-cache-$CI_JOB_IMAGE
  paths:
    - target/release/
    - target/wasm32-unknown-unknown/release/
    - Cargo.lock
  policy: pull-push

build-test-debug-native:cargo:
  cache:
  - <<: *crates-io-cache
    policy: pull
  - <<: *target-debug-native-cache
  script:
  - rustup target add wasm32-unknown-unknown
  - rustc --version && cargo --version
  - cargo build --lib --tests
  - make test-native

build-test-debug-wasm:cargo:
  variables:
    TMPDIR: $CI_PROJECT_DIR
  cache:
  - <<: *crates-io-cache
    policy: pull
  - <<: *target-debug-wasm-cache
  script:
    # wasm-bindgen does not seem to be handling RUSTFLAGS properly,
    # causing spurious rebuilds due to cargo complaining that the
    # variable has changed. To work around that problem, just unset it
    # here. Sigh.
  - unset RUSTFLAGS
  - rustup target add wasm32-unknown-unknown
  - rustc --version && cargo --version
  - cargo build --lib --tests --target=wasm32-unknown-unknown
    # firefox-geckodriver is only available on Ubuntu. So make its
    # repository known.
  - echo 'deb http://us.archive.ubuntu.com/ubuntu/ focal main universe' >> /etc/apt/sources.list
  - apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 871920D1991BC93C
  - apt-get update
    # Node.js is the default test backend and we don't change it for the
    # unit tests. All other tests run in a headless browser, we use
    # Firefox.
  - apt-get install --no-upgrade --assume-yes nodejs firefox-geckodriver
  - make test-runner
  - make test-wasm

build-release-native:cargo:
  cache:
  - <<: *crates-io-cache
    policy: pull
  - <<: *target-release-native-cache
  script:
  - rustc --version && cargo --version
  - cargo build --lib --tests --release

build-release-wasm:cargo:
  cache:
  - <<: *crates-io-cache
  - <<: *target-release-wasm-cache
  script:
  - unset RUSTFLAGS
  - rustup target add wasm32-unknown-unknown
  - rustc --version && cargo --version
  - cargo build --lib --tests --release --target=wasm32-unknown-unknown

lint:clippy:
  script:
  - rustup component add clippy
  # TODO: Once we removed deprecated features we should remove the -A
  #       part.
  - cargo clippy --all-targets --all-features --tests -- -A unknown_lints -A deprecated -D warnings
